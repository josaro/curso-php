<?php
  include_once('include_fns.php');

  if ( (!isset($_REQUEST['username'])) || (!isset($_REQUEST['password'])) )
  {
    echo 'Debes escribir tu nombre de usuario y contraseņa para proceder';
    exit;
  }

  $username = $_REQUEST['username'];
  $password = $_REQUEST['password'];

  if (login($username, $password))
  {
    $_SESSION['auth_user'] = $username;
    header('Location: '.$_SERVER['HTTP_REFERER']);
  }
  else
  {
    echo 'La contraseņa que has escrito no es correcta';
    exit;
  }
?>
