create table clientes
( clienteid int unsigned not null auto_increment primary key,
  nombre char(30) not null,
  direccion char(40) not null,
  ciudad char(20) not null
);

create table pedidos
( pedidoid int unsigned not null auto_increment primary key,
  clienteid int unsigned not null,
  cantidad float(6,2),
  fecha date not null
);

create table libros
(  isbn char(13) not null primary key,
   autor char(30),
   titulo char(60),
   precio float(4,2)
);

create table articulos_pedidos
( pedidoid int unsigned not null,
  isbn char(13) not null,
  cantidad tinyint unsigned,

  primary key (pedidoid, isbn)
 
);

create table comentarios_libros
(
  isbn char(13) not null primary key,
  comentario text
);
