<?php
class A
{
        var $atributo ="valor por defecto";
        function operacion()
        {
                echo "Algo<br>";
                echo "El valor de \$atributo es $this->atributo<br>";
                }
        }

class B extends A
{
        var $atributo="otro valor diferente";
        function operacion()
        {
                echo "Cualquier otra cosa<br>";
                echo "El valor de \$atributo es $this->atributo<br>";
                }
        }
$b=new B;
$b->operacion();


?>