use libreria;

insert into clientes values
  (NULL, "Carmen G�mez", "Avda. Rioja, 25", "M�laga"),
  (NULL, "Alberto Durante", "Calle Magnolias, 12", "Benavente"),
  (NULL, "Miguel Ant�nez", "Carretera Aeropuerto, 23", "Bilbao");

insert into pedidos values
  (NULL, 3, 69.98, "02-Abr-2000"),
  (NULL, 1, 49.99, "15-Abr-2000"),
  (NULL, 2, 74.98, "19-Abr-2000"),
  (NULL, 3, 24.99, "01-May-2000");

insert into libros values
  ("0-672-31697-8", "Miguel Mor�n", "Flash para desarrollares profesionales", 34.99),
  ("0-672-31745-1", "Tom�s Ligero", "Instalar Sistemas Operativos", 24.99),
  ("0-672-31509-2", "Ernesto del Ponte.", "Aprender Power Point es muy f�cil", 24.99),
  ("0-672-31769-9", "Alberto Moravia", "El dise�o gr�fico aplicado a la web", 49.99);

insert into articulos_pedidos values
  (1, "0-672-31697-8", 2),
  (2, "0-672-31769-9", 1),
  (3, "0-672-31769-9", 1),
  (3, "0-672-31509-2", 1),
  (4, "0-672-31745-1", 3);

insert into comentarios_libros values
  ("0-672-31697-8", "El libro de Miguel Mor�n con conceptos muy claros pero al tiempo cubriendo los temas m�s complejos sobre Flash y ActionScript de �ltima generaci�n.");
