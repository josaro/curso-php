<?
  require ("pagina.inc");

  class PaginaServicios extends Pagina
  {
    var $fila2Botones = array( "Descargas" => "descargas.php",
                              "Materiales Online" => "online.php",
                              "Servicios Personalizados" => "personales.php",
                              "Nuestras Ofertas" => "ofertas.php"
                            );
    function Mostrar()
    {
      echo "<html>\n<head>\n";
      $this -> MostrarTitulo();
      $this -> MostrarPalabrasClave();
      $this -> MostrarEstilos();
      echo "</head>\n<body>\n";
      $this -> MostrarCabecera();
      $this -> MostrarMenu($this->botones);
      $this -> MostrarMenu($this->fila2Botones);
      echo $this->contenidos;
      $this -> MostrarFooter();
      echo "</body>\n</html>\n";
    }
  }

  $servicios = new PaginaServicios();
  $contenidos ="<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
  <tr>
    <th bgcolor=\"#000000\" scope=\"col\"><img src=\"Images/contenido.png\" width=\"780\" height=\"442\"></th>
  </tr>
</table>";
  $servicios -> SetContenidos($contenidos);
  $servicios -> Mostrar();
?>