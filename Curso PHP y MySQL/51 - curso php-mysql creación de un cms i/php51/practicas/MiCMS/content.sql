DROP DATABASE IF EXISTS content;

CREATE DATABASE content;

USE content;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `keywords`
--

CREATE TABLE `keywords` (
  `story` int(11) NOT NULL default '0',
  `keyword` varchar(32) NOT NULL default '',
  `weight` int(11) NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `keywords`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pages`
--

CREATE TABLE `pages` (
  `code` varchar(16) NOT NULL default '',
  `description` text,
  PRIMARY KEY  (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `pages`
--

INSERT INTO `pages` VALUES ('flash', 'Noticias más destacadas del Mundo Flash');
INSERT INTO `pages` VALUES ('photoshop', 'Noticias Más destacadas del Mundo Photoshop.');
INSERT INTO `pages` VALUES ('php', 'Noticias más destacadas del Mundo PHP');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stories`
--

CREATE TABLE `stories` (
  `id` int(11) NOT NULL auto_increment,
  `writer` varchar(16) NOT NULL default '',
  `page` varchar(16) NOT NULL default '',
  `headline` text,
  `story_text` text,
  `picture` text,
  `created` int(11) default NULL,
  `modified` int(11) default NULL,
  `published` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcar la base de datos para la tabla `stories`
--

INSERT INTO `stories` VALUES (1, 'pedro', 'flash', 'Macromedia anuncia nueva versión: Studio 8', 'Se trata de la última versión (de momento) de Dreamweaver®, Flash® Professional, Fireworks®, Contribute™ and FlashPaper™, Studio 8 ofrece a los diseñadores web y desarrolladores un nuevo nivel de expresividad, eficiencia y simplicidad en el trabajo en grupo para crear sitios web, experiencias interactivas y contenido móvil. Demasiadas versiones, demasiado rápido. Parece tener un sentido exclusivamente comercial, aunque otros lo ven como una maniobra (de momento difícil de explicar) ante el proceso de fusión (o absorción) entre Macromedia y Adobe. Podéis ver las novedades en profundidad en el sitio oficial de macromedia, así como una precompra. ', 'images/1.jpg', 976573221, 1123700516, 976570230);
INSERT INTO `stories` VALUES (2, 'pedro', 'flash', 'Excelente sitio con VideoTutoriales sobre Flash ( Pero en inglés)', 'Flashextensions proporciona clases particulares en formato video de calidad sobre flash y a otras tecnologías relacionadas. El contenido está dirigido a quienes comienzan a construir aplicaciones con flash, a los que hacen la transición de una lengua o de una plataforma anterior a flash (es decir Java o NET), y/o los que deseen aprender nuevas técnicas de programación en la plataforma flash.', 'images/2.jpg', 976562355, 1123700707, 976570230);
INSERT INTO `stories` VALUES (3, 'pedro', 'flash', 'Flashdevelop. Editor ActionScript gratuito', '<body>\r\n<P>Un bonito proyecto, <A \r\nhref=\\"http://www.meychi.com/archive/000036.php\\">FlashDevelop</A> es un editor ampliable a varios lenguajes programado en C# y gratuito. Cuenta con <A \r\nhref=\\"http://www.meychi.com/forum/\\">forums</A> para las dudas y personalizable por medio de templates.<BR>\r\n  Necesita el Microsoft.NET 1.1 Framework :/<BR>\r\n  Puedes bajarlo y probarlo <A href=\\"http://www.meychi.com/source/programs/\\">desde aqu&iacute;</A>.<BR>\r\n  Por otro lado veremos que novedades ofrece el proyecto de macromedia <A \r\nhref=\\"http://radar.oreilly.com/archives/2005/06/nextgen_macrome.html\\">Zorn</A> (editor FLEX y AS)</P>\r\n</body>', 'images/3.jpg', 976542355, 1123701165, 976555650);
INSERT INTO `stories` VALUES (4, 'ana', 'photoshop', 'Teclados especiales para Photoshop', '¿Te has preguntado alguna vez cómo aprender más rápidamente todos los atajos de teclado de Photoshop?\r\nBueno, en realidad, aprenderlos todos es casi imposible. Los hay a centenares. Así pues, una de las soluciones es disponer de un teclado que esté diseñado específicamente para no tener que recordar los comandos más frecuentes, y algunos que no lo son tanto.', 'images/4.jpg', 976531355, 1123701718, 976533320);
INSERT INTO `stories` VALUES (5, 'ana', 'photoshop', 'Adobe Photoshop CS2 en castellano', 'Adobe Photoshop CS2 es el software estándar de edición de imágenes profesional y el líder de la gama de productos de edición de imágenes digitales que aporta más de lo que usted se espera. Las innovadoras herramientas creativas le ayudan a conseguir resultados excepcionales. Una adaptabilidad sin precedentes le permite personalizar Photoshop de acuerdo con su método de trabajo. Además, gracias a unos procesos de edición, tratamiento y gestión de archivos más eficaces podrá trabajar con mayor rapidez.', 'images/5.jpg', 976542355, 1123701586, 976555650);
INSERT INTO `stories` VALUES (6, 'pedro', 'php', 'PHPBuilder.com ', 'Uno de los mejores sitios con recursos PHP. Tutoriales, plantillas, Librerías de códigos, noticias, artículos. Sus tutoriales son sin duda de los mejores y más en profundidad que se pueden encontrar en la red además de ser muy, muy numerosos y variados. En inglés.', 'images/6.jpg', 976542355, 1123701286, 976555650);
INSERT INTO `stories` VALUES (7, 'pedro', 'php', 'PHP-Nuke 7.8. Nueva versión del CMS más popular', 'La versión final de PHP-Nuke 7.8 ha sido publicada. Incluye un sistema totalmente nuevo de publicidad que hará tu sitio más productivo. El nuevo sistema de publicidad es muy sencillo de usar y muy potente, admite anuncios en javascript o código HTML, enlaces de texto, películas flash y gráficos tradicionales. También se ha creado un nuevo módulo de publicidad para exponer tus precios/ofertas y donde tus clientes pueden loguearse para ver sus estadísticas en tiempo real. Han sido arreglados algunos bugs en el nuevo editor, incluido BBtoNuke 2.0.15, mejores en el sistema de bloques, alguna modificación menor en el panel de administración y arreglados varios bugs. Adicionalmente la versión 7.7 de PHP-Nuke ha sido puesta para descarga libre del público en la sección de descargas del sitio oficial de PHP-Nuke.', 'images/7.jpg', 976451129, 1123701372, 976458754);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `writer_permissions`
--

CREATE TABLE `writer_permissions` (
  `writer` varchar(16) NOT NULL default '',
  `page` varchar(16) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `writer_permissions`
--

INSERT INTO `writer_permissions` VALUES ('ana', 'flash');
INSERT INTO `writer_permissions` VALUES ('ana', 'photoshop');
INSERT INTO `writer_permissions` VALUES ('pedro', 'flash');
INSERT INTO `writer_permissions` VALUES ('pedro', 'php');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `writers`
--

CREATE TABLE `writers` (
  `username` varchar(16) NOT NULL default '',
  `password` varchar(40) NOT NULL default '',
  `full_name` text,
  PRIMARY KEY  (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `writers`
--

INSERT INTO `writers` VALUES ('ana', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Ana Robles');
INSERT INTO `writers` VALUES ('pedro', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Pedro Romero');
