<?php
  // writer.php Is the Interface for Writers to Manage Their Stories

  include_once('include_fns.php');

  if (!check_auth_user())
  {
    login_form();
  }
  else
  {
    $handle = db_connect();

    $writer = get_writer_record($_SESSION['auth_user']);

    echo '<p>Bienvenid@, '.$writer['full_name'];
    echo ' (<a href="logout.php">Logout</a>) (<a href="index.php">Men�</a>) (<a href="../">Sitio P�blico</a>) </p>';
    echo '<p>';

    $query = 'select * from stories where writer = \''.
           $_SESSION['auth_user'].'\' order by created desc';
    $result = $handle->query($query);

    echo 'Tus Historias: ';
    echo $result->num_rows;
    echo ' (<a href="story.php">A�adir Noticia</a>)';
    echo '</p><br /><br />';

    if ($result->num_rows)
    {
      echo '<table>';
      echo '<tr><th>Titular</th><th>P�gina</th>';
      echo '<th>Creada</th><th>Ultima Modif.</th></tr>';
      while ($stories = $result->fetch_assoc())
      {
        echo '<tr><td>';
        echo $stories['headline'];
        echo '</td><td>';
        echo $stories['page'];
        echo '</td><td>';
        echo date('d /m, H:i', $stories['created']);
        echo '</td><td>';
        echo date('d / m, H:i', $stories['modified']);
        echo '</td><td>';
        if ($stories['published'])
        {
          echo '[Publicado '.date('d M, H:i', $stories['published']).']';
        }
        else
        {
          echo '[<a href="story.php?story='.$stories['id'].'">editar</a>] ';
          echo '[<a href="delete_story.php?story='.$stories['id'].'">borrar</a>] ';
        }
        echo '[<a href="keywords.php?story='.$stories['id'].'">palabras clave</a>]';
        echo '</td></tr>';
      }
      echo '</table>';
    }
  }
?>
