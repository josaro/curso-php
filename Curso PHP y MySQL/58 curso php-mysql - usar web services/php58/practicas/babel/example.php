<?php
/* simple example */
include("babelfish.class.php");
$message="I am a multilingual guy !";
echo "In English: ".$message.'<br>';
/* babel class usage */
$tr=new babelfish();
echo "In French: ".$tr->translate($message,'english','French').'<br>';
echo "In Dutch: ".$tr->translate($message,'english','dutch').'<br>';
?>
