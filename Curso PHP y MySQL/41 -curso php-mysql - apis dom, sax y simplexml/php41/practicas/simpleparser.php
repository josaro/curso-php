<?php
$file = "receta.xml";

// Llama esto al comienzo de cada elemento
function startElement($parser, $name, $attrs) {
    print "<B>$name =></B>  ";
}

// Llama esto al final de cada elemento
function endElement($parser, $name) {
    print "\n";
}

// Llama esto en todos los sitios donde aparezcan datos carácter
function characterData($parser, $value) {
    print "$value<BR>";
}

// Define el analizador
$simpleparser = xml_parser_create();
xml_set_element_handler($simpleparser, "startElement", "endElement");
xml_set_character_data_handler($simpleparser, "characterData");

// Abre el archivo XML para leer
if (!($fp = fopen($file, "r"))) {
  die("could not open XML input");
}

// Analízalo
while ($data = fread($fp, filesize($file))) {
if (!xml_parse($simpleparser, $data, feof($fp))) {
  die(xml_error_string(xml_get_error_code($simpleparser)));
  }
}

// Libera memoria
xml_parser_free($simpleparser);
?>
