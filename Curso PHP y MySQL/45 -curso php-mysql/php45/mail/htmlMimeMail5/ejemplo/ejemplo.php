<?php
/**
* This file is part of the htmlMimeMail5 package (http://www.phpguru.org/)
*
* htmlMimeMail5 is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* htmlMimeMail5 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with htmlMimeMail5; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* � Copyright 2005 Richard Heyes
*/

    require_once('../htmlMimeMail5.php');

    $mail = new htmlMimeMail5();

    /**
    * Configurar la direcci�n desde donde se env�a
    */
    $mail->setFrom('S�ptimo Continente <admin@septimocontinente.com>');

    /**
    * Configurar el asunto
    */
    $mail->setSubject('email de Prueba');

    /**
    * Configurar alta prioridad
    */
    $mail->setPriority('high');

    /**
    * Configurar el texto del Email
    */
    $mail->setText('Ejemplo de Texto');

    /**
    * Configurar el HTML del email
    */
    $mail->setHTML('<b>Ejemplo HTML</b> <img src="background.gif">');

    /**
    * A�adir una imagen embebida
    */
    $mail->addEmbeddedImage(new fileEmbeddedImage('background.gif'));

    /**
    * A�adir un adjunto
    */
    $mail->addAttachment(new fileAttachment('ejemplo.zip'));

    /**
    * Send the email
    */
    $mail->send(array('admin@septimocontinente.com'));
?>
