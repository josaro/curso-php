<?
  include ('book_sc_fns.php');
  // El carrito de compra necesita sesiones, as� que empezar una
  session_start();

  $name = get_category_name($catid);

  do_html_header($name);

  // obener la informaci�n del libro de la base de datos
  $book_array = get_books($catid);

  display_books($book_array);


  // si est� loqueado como admin, mostrar enlaces a�adir y borrar libros
  if(session_is_registered("admin_user"))
  {
    display_button("index.php", "continue", "Continue Shopping");
    display_button("admin.php", "admin-menu", "Admin Menu");
    display_button("edit_category_form.php?catid=$catid", "edit-category", "Edit Category");
  }
  else
    display_button("index.php", "continue-shopping", "Continue Shopping");

  do_html_footer();
?>