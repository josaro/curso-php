<?

// incluye archivos de funci�n para esta aplicaci�n
require_once("book_sc_fns.php");
session_start();

do_html_header("Editar categor�a");
if (check_admin_user())
{
  if ($catname = get_category_name($catid))
  {
    $cat = compact("catname", "catid");
    display_category_form($cat);
  }
  else
    echo "No se han podido recuperar los detalles de la categor�a.<br>";
  do_html_url("admin.php", "Volver al men� de administraci�n");
}
else
  echo "No est�s autorizado a entrar en el �rea de administraci�n.";

do_html_footer();

?>