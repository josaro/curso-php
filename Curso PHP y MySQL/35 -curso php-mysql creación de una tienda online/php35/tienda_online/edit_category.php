<?

// incluye archivos de funci�n para esta aplicaci�n
require_once("book_sc_fns.php");
session_start();

do_html_header("Actualizar categor�a");
if (check_admin_user())
{
  if (filled_out($HTTP_POST_VARS))
  {
    if(update_category($catid, $catname))
      echo "La Categor�a ha sido actualizada.<br>";
    else
      echo "La Categor�a no ha podido ser actualizada.<br>";
  }
  else
    echo "No has cubierto el formulario.  Prueba de nuevo por favor.";
  do_html_url("admin.php", "Volver al men� de administraci�n");
}
else
  echo "No est�s autorizado a ver esta p�gina.";

do_html_footer();

?>