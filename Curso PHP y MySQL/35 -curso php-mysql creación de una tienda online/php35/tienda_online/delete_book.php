<?

// incluye archivos de funci�n para esta aplicaci�n
require_once("book_sc_fns.php");
session_start();

do_html_header("Deleting book");
if (check_admin_user())
{
  if ($isbn)
  {
    if(delete_book($isbn))
      echo "Libro '$isbn' ha sido borrado.<br>";
    else
      echo "Libro '$isbn' no ha podido ser actualizado.<br>";
  }
  else
    echo "Necesitamos un ISBN para borrar un libro.  Por favor int�ntalo de nuevo.<br>";
  do_html_url("admin.php", "Volver al men� de administraci�n");
}
else
  echo "No est�s autorizado a ver esta p�gina.";

do_html_footer();

?>