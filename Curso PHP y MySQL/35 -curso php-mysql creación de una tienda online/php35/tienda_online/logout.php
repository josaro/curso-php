<?

// incluir archivos de funciones para esta aplicaci�n
require_once("book_sc_fns.php");
session_start();
$old_user = $admin_user;  // almacenar para comprobar si  *estuvieron* logged in
$result_unreg = session_unregister("admin_user");
$result_dest = session_destroy();

// Empezar salida html
do_html_header("Logging Out");

if (!empty($old_user))
{
  if ($result_unreg && $result_dest)
  {
    // si ellos estuvieron logged in y ahora est�n logged out
    echo "Logged out.<br>";
    do_html_url("login.php", "Login");
  }
  else
  {
   // ellos estuvieron logged in y no han podido estar logged out
    echo "No ha podido hacer log out.<br>";
  }
}
else
{
  // si ellos no estaban logged in pero llega a esta p�gina de alg�n modo
  echo "Usted no estaba logged in, y por tanto no puede hacer logged out.<br>";
  do_html_url("login.php", "Login");
}

do_html_footer();

?>