<?
function calculate_shipping_cost()
{
  // como estamos enviando productos a todo el mundo
  // via teleportation, gastos env�o son fijos
  return 20.00;
}

function get_categories()
{
   // Petici�n a la base de datos de una lista de categor�as
   $conn = db_connect();
   $query = "select catid, catname
             from categories";
   $result = @mysql_query($query);
   if (!$result)
     return false;
   $num_cats = @mysql_num_rows($result);
   if ($num_cats ==0)
      return false;
   $result = db_result_to_array($result);
   return $result;
}

function get_category_name($catid)
{
   // Petici�n a la base de datos del nombre de una categor�a id
   $conn = db_connect();
   $query = "select catname
             from categories
             where catid = $catid";
   $result = @mysql_query($query);
   if (!$result)
     return false;
   $num_cats = @mysql_num_rows($result);
   if ($num_cats ==0)
      return false;
   $result = mysql_result($result, 0, "catname");
   return $result;
}


function get_books($catid)
{
   // petici�n a la base de datos de los libros de una categor�a
   if (!$catid || $catid=="")
     return false;

   $conn = db_connect();
   $query = "select * from books where catid='$catid'";
   $result = @mysql_query($query);
   if (!$result)
     return false;
   $num_books = @mysql_num_rows($result);
   if ($num_books ==0)
      return false;
   $result = db_result_to_array($result);
   return $result;
}

function get_book_details($isbn)
{
  // Petici�n a la base de datos por todos los detalles de un libro particular
  if (!$isbn || $isbn=="")
     return false;

   $conn = db_connect();
   $query = "select * from books where isbn='$isbn'";
   $result = @mysql_query($query);
   if (!$result)
     return false;
   $result = @mysql_fetch_array($result);
   return $result;
}

function calculate_price($cart)
{
  // suma total de los precios de todos los art�culos en el carrito de compras
  $price = 0.0;
  if(is_array($cart))
  {
    $conn = db_connect();
    foreach($cart as $isbn => $qty)
    {
      $query = "select price from books where isbn='$isbn'";
      $result = mysql_query($query);
      if ($result)
      {
        $item_price = mysql_result($result, 0, "price");
        $price +=$item_price*$qty;
      }
    }
  }
  return $price;
}

function calculate_items($cart)
{
  // suma total de los art�culos en el carrito de compras
  $items = 0;
  if(is_array($cart))
  {
    foreach($cart as $isbn => $qty)
    {
      $items += $qty;
    }
  }
  return $items;
}

?>