<?

// incluye archivos de función para esta aplicación
require_once("book_sc_fns.php");
session_start();

do_html_header("Editar detalles libro");
if (check_admin_user())
{
  if ($book = get_book_details($isbn))
  {
    display_book_form($book);
  }
  else
    echo "No se han podido recuperar los detalles del libro.<br>";
  do_html_url("admin.php", "Volver al menú de Administración");
}
else
  echo "No estás autorizado para entrar en el área de administración.";

do_html_footer();

?>