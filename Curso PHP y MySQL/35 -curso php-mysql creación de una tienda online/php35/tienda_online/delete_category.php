<?

// incluye archivos de funci�n para esta aplicaci�n
require_once("book_sc_fns.php");
session_start();

do_html_header("Borrando categor�a");
if (check_admin_user())
{
  if ($catid)
  {
    if(delete_category($catid))
      echo "La Categor�a ha sido borrada.<br>";
    else
      echo "La Categor�a no ha podido ser borrada.<br>"
           ."Esto suele ser porque no se encuentra vac�a.<br>";
  }
  else
    echo "No ha especificado categor�a.  Prueba de nuevo por favor.<br>";
  do_html_url("admin.php", "Volver al men� de Administraci�n");
}
else
  echo "No est�s autorizado a ver esta p�gina.";

do_html_footer();

?>