<?
  //incluye nuestro conjunto de funciones
  include ('book_sc_fns.php');

  // El carrito de compra necesita sesiones, as� que empezar una
  session_start();

  do_html_header("Caja de pago");

  if($cart&&array_count_values($cart))
  {
    display_cart($cart, false, 0);
    display_checkout_form();
  }
  else
    echo "<p>No hay art�culos en su carrito";

  display_button("show_cart.php", "continue-shopping", "Continue Shopping");

  do_html_footer();
?>