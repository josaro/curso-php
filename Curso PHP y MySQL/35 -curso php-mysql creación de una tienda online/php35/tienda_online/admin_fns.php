<?
// Este archivo contiene las funciones usadas por la interface de admin
// del carrito de compra de la Librer�a Online.

function display_category_form($category = "")
// Esto muestra el formulario de categor�a.
// Este formulario puede ser usado para insertar o editar categor�as.
// Para insertar, no pasar ning�n par�metro. Esto configurar� $edit
// como falso, y el formulario ir� a insert_category.php.
// Para actualizar, pasa un array conteniendo una categor�a.  El
// formulario contendr� los viejos datos y  apuntar� a update_category.php.
// Tambi�n a�adir� un bot�n "Borrar categor�a" .
{
  // si se pasa una categor�a existente, proceder en "modo edici�n"
  $edit = is_array($category);

  // la mayor�a del formulario esta en HTML plano con alg�n
  // trozo opcional de PHP por todo.
?>
  <form method=post
      action="<?=$edit?"edit_category.php":"insert_category.php";?>">
  <table border=0>
  <tr>
    <td>Nombre Categor�a:</td>
    <td><input type=text name=catname size=40 maxlength=40
          value="<?=$edit?$category["catname"]:""; ?>"></td>
   </tr>
  <tr>
    <td <? if (!$edit) echo "colspan=2"; ?> align=center>
      <? if ($edit)
         echo "<input type=hidden name=catid
                value=\"".$category["catid"]."\">";
      ?>
      <input type=submit
       value="<?=$edit?"Actualizar":"A�adir"; ?> Categor�a"></form>
     </td>
     <? if ($edit)
       // permitir borrar categor�as existentes
       {
          echo "<td>";
          echo "<form method=post action=\"delete_category.php\">";
          echo "<input type=hidden name=catid value=\"".$category[catid]."\">";
          echo "<input type=submit value=\"Borrar Categor�a\">";
          echo "</form></td>";
       }
     ?>
  </tr>
  </table>
<?
}

function display_book_form($book = "")
// Muestra el formulario libro.
// Es muy similar al formulario categor�a.
// Este formulario puede ser usado para insertar o editar libros..
// Para insertar, no pasar ning�n par�metro.  Configurar� $edit
// como falso, y el formulario ir� a insert_book.php.
// Para actualizar, pasar un array conteniendo un libro. El
// formulario ser� mostrado con los viejos datos y apuntar� a update_book.php.
// Tambi�n a�adir� un bot�n "Borrar libro".
{

  // si es pasado un libro existente, proceder en "modo edici�n"
  $edit = is_array($book);

  // la mayor�a del formulario est� en HTML plano con alg�n
  // trozo de PHP opcional
?>
  <form method=post
        action="<?=$edit?"edit_book.php":"insert_book.php";?>">
  <table border=0>
  <tr>
    <td>ISBN:</td>
    <td><input type=text name=isbn
         value="<?=$edit?$book["isbn"]:""; ?>"></td>
  </tr>
  <tr>
    <td>T�tulo Libro:</td>
    <td><input type=text name=title
         value="<?=$edit?$book["title"]:""; ?>"></td>
  </tr>
  <tr>
    <td>Autor Libro:</td>
    <td><input type=text name=author
         value="<?=$edit?$book["author"]:""; ?>"></td>
   </tr>
   <tr>
      <td>Categor�a:</td>
      <td><select name=catid>
      <?
          // lista de las categor�as posibles enviadas por la base de datos
          $cat_array=get_categories();
          foreach ($cat_array as $thiscat)
          {
               echo "<option value=\"";
               echo $thiscat["catid"];
               echo "\"";
               // si existen libros, ponerlo en la categor�a actual
               if ($edit && $thiscat["catid"] == $book["catid"])
                   echo " selected";
               echo ">";
               echo $thiscat["catname"];
               echo "\n";
          }
          ?>
          </select>
        </td>
   </tr>
   <tr>
    <td>Precio:</td>
    <td><input type=text name=price
               value="<?=$edit?$book["price"]:""; ?>"></td>
   </tr>
   <tr>
     <td>Descripci�n:</td>
     <td><textarea rows=3 cols=50
          name=description>
          <?=$edit?$book["description"]:""; ?>
          </textarea></td>
    </tr>
    <tr>
      <td <? if (!$edit) echo "colspan=2"; ?> align=center>
         <?
            if ($edit)
             // necesitamos el viejo isbn para encontrar libros en la base de datos
             // si el isbn est� siendo actualizado
             echo "<input type=hidden name=oldisbn
                    value=\"".$book["isbn"]."\">";
         ?>
        <input type=submit
               value="<?=$edit?"Update":"Add"; ?> Book">
        </form></td>
        <?
           if ($edit)
           {
             echo "<td>";
             echo "<form method=post action=\"delete_book.php\">";
             echo "<input type=hidden name=isbn
                    value=\"".$book["isbn"]."\">";
             echo "<input type=submit
                    value=\"Borrar libro\">";
             echo "</form></td>";
            }
          ?>
         </td>
      </tr>
  </table>
  </form>
<?
}

function display_password_form()
{
// muestra el formulario para cambiar la contrase�a
?>
   <br>
   <form action="change_password.php" method=post>
   <table width=250 cellpadding=2 cellspacing=0 bgcolor=#cccccc>
   <tr><td>Vieja contrase�a:</td>
       <td><input type=password name=old_passwd size=16 maxlength=16></td>
   </tr>
   <tr><td>Nueva Contrase�a:</td>
       <td><input type=password name=new_passwd size=16 maxlength=16></td>
   </tr>
   <tr><td>Repetir Nueva Contrase�a:</td>
       <td><input type=password name=new_passwd2 size=16 maxlength=16></td>
   </tr>
   <tr><td colspan=2 align=center><input type=submit value="Cambiar Contrase�a">
   </td></tr>
   </table>
   <br>
<?
};

function insert_category($catname)
// inserta una nueva categor�a en la base de datos
{
   $conn = db_connect();

   // comprueba que la categor�a no exista ya
   $query = "select *
             from categories
             where catname='$catname'";
   $result = mysql_query($query);
   if (!$result || mysql_num_rows($result)!=0)
     return false;

   // inserta la nueva categor�a
   $query = "insert into categories values
            ('', '$catname')";
   $result = mysql_query($query);
   if (!$result)
     return false;
   else
     return true;
}

function insert_book($isbn, $title, $author, $catid, $price, $description)
// inserta un nuevo libro en la base de datos
{
   $conn = db_connect();

   // comprueba que la categor�a no exista ya
   $query = "select *
             from books
             where isbn='$isbn'";
   $result = mysql_query($query);
   if (!$result || mysql_num_rows($result)!=0)
     return false;
   // inserta nueva categor�a
   $query = "insert into books values
            ('$isbn', '$author', '$title', '$catid', '$price', '$description')";
   $result = mysql_query($query);
   if (!$result)
     return false;
   else
     return true;
}

function update_category($catid, $catname)
// cambia el nombre de la categor�a con catid en la base de datos
{
   $conn = db_connect();

   $query = "update categories
             set catname='$catname'
             where catid='$catid'";
   $result = @mysql_query($query);
   if (!$result)
     return false;
   else
     return true;
}

function update_book($oldisbn, $isbn, $title, $author, $catid,
                     $price, $description)
// cambia los detalles de los libros almacenados bajo $oldisbn en
// la base de datos con nuevos detalles en argumentos
{
   $conn = db_connect();

   $query = "update books
             set isbn='$isbn',
             title ='$title',
             author = '$author',
             catid = '$catid',
             price = '$price',
             description = '$description'
             where isbn='$oldisbn'";

   $result = @mysql_query($query);
   if (!$result)
     return false;
   else
     return true;
}

function delete_category($catid)
// Elimina la categor�a identificada por catid de la base de datos
// Si hay libros en la categor�a, no
// ser� eliminada y la funci�n devolvera falso.
{
   $conn = db_connect();

   // comprueba si hay algunos libros en la categor�a
   // para evitar borrados an�malos
   $query = "select *
             from books
             where catid='$catid'";
   $result = @mysql_query($query);
   if (!$result || @mysql_num_rows($result)>0)
     return false;

   $query = "delete from categories
             where catid='$catid'";
   $result = @mysql_query($query);
   if (!$result)
     return false;
   else
     return true;
}


function delete_book($isbn)
// Borra el libro identificado por $isbn de la base de datos.
{
   $conn = db_connect();

   $query = "delete from books
             where isbn='$isbn'";
   $result = @mysql_query($query);
   if (!$result)
     return false;
   else
     return true;
}

?>