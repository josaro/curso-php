<?

// incluye archivos de funci�n para esta aplicaci�n
require_once("book_sc_fns.php");
session_start();

do_html_header("Actualizar libro");
if (check_admin_user())
{
  if (filled_out($HTTP_POST_VARS))
  {
    if(update_book($oldisbn, $isbn, $title, $author, $catid,
                      $price, $description))
      echo "Libro ha sido actualizado.<br>";
    else
      echo "Libro no ha podido ser actualizado.<br>";
  }
  else
    echo "No has cubierto el formulario.  Prueba de nuevo por favor.";
  do_html_url("admin.php", "Volver al men� de administraci�n");
}
else
  echo "No est�s autorizado a ver esta p�gina.";

do_html_footer();

?>