<?
 require_once("book_sc_fns.php");
 session_start();
 do_html_header("Cambiar Contrase�a");
 check_admin_user();
 if (!filled_out($HTTP_POST_VARS))
 {
   echo "No has cubierto el formulario completamente.
         Int�ntalo de nuevo, por favor.";
   do_html_url("admin.php", "Volver al men� de administraci�n");
   do_html_footer();
   exit;
 }
 else
 {
    if ($new_passwd!=$new_passwd2)
       echo "Contrase�a escrita no era la misma.  No se ha cambiado.";
    else if (strlen($new_passwd)>16 || strlen($new_passwd)<6)
       echo "La Nueva contrase�a debe estar entre 6 y 16 caracteres.  Prueba de nuevo.";
    else
    {
        // attempt update
        if (change_password($admin_user, $old_passwd, $new_passwd))
           echo "Contrase�a cambiada.";
        else
           echo "La Contrase�a no ha podido cambiarse.";
    }


 }
  do_html_url("admin.php", "Volver al men� de administraci�n");
  do_html_footer();
?>