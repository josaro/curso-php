<?

  include ('book_sc_fns.php');
  // El carro de compra necesita sesiones, as� que empezar una
  session_start();

  do_html_header("Caja de pago");

  // si cubierto
  if($cart&&$name&&$address&&$city&&$zip&&$country)
  {
    // se ha podido insertar en la base de datos
    if( insert_order($HTTP_POST_VARS)!=false )
    {
      //mostrar carro, no permitir cambios y sin im�genes
      display_cart($cart, false, 0);

      display_shipping(calculate_shipping_cost());

      //obtener detalles de la tarjeta de cr�dito
      display_card_form($name);

      display_button("show_cart.php", "continue-shopping", "Continue Shopping");
    }
    else
    {
      echo "Could not store data, please try again.";
      display_button("checkout.php", "back", "Back");
    }
  }
  else
  {
    echo "You did not fill in all the fields, please try again.<hr>";
    display_button("checkout.php", "back", "Back");
  }

  do_html_footer();
?>