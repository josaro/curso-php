<?
  include ('book_sc_fns.php');
  // El carro de compra necesita sesiones, as� que empezar una
  session_start();

  // obtener este libro de la base de datos
  $book = get_book_details($isbn);
  do_html_header($book["title"]);
  display_book_details($book);

  // configurar url para "bot�n continuar"
  $target = "index.php";
  if($book["catid"])
  {
    $target = "show_cat.php?catid=".$book["catid"];
  }
  // si est�s logueado como un admin, mostrar enlaces editar libro
  if( check_admin_user() )
  {
    display_button("edit_book_form.php?isbn=$isbn", "edit-item", "Edit Item");
    display_button("admin.php", "admin-menu", "Admin Menu");
    display_button($target, "continue", "Continue");
  }
  else
  {
    display_button("show_cart.php?new=$isbn", "add-to-cart", "Add ".$book["title"]." To My Shopping Cart");
    display_button($target, "continue-shopping", "Continue Shopping");
  }

  do_html_footer();
?>