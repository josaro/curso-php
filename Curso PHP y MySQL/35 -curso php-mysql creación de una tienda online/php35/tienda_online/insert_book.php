<?

// incluir archivos de funci�n para esta aplicaci�n
require_once("book_sc_fns.php");
session_start();

do_html_header("A�adir un libro");
if (check_admin_user())
{
  if (filled_out($HTTP_POST_VARS))
  {
    if(insert_book($isbn, $title, $author, $catid, $price, $description))
      echo "Libro '$title' ha sido a�adido a la base de datos.<br>";
    else
      echo "Libro '$title' no ha podido ser a�adido a la base de datos.<br>";
  }
  else
    echo "No has cubierto el formulario.  Prueba de nuevo por favor.";
  do_html_url("admin.php", "Volver al men� de administraci�n");
}
else
  echo "No est�s autorizado a ver esta p�gina.";

do_html_footer();

?>