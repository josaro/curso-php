<?

// incluir archivos de funciones para esta aplicaci�n
require_once("book_sc_fns.php");
session_start();

do_html_header("A�adir una categor�a");
if (check_admin_user())
{
  display_category_form();
  do_html_url("admin.php", "Volver al men� de administraci�n");
}
else
  echo "No est�s autorizado a entrar en el �rea de administraci�n.";

do_html_footer();

?>