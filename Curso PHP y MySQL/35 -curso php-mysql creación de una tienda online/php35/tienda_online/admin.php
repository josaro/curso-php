<?

// incluye los archivos de funci�n para esta aplicaci�n
require_once("book_sc_fns.php");
session_start();

if ($username && $passwd)
// est�n intentando hacer logging in
{
    if (login($username, $passwd))
    {
      // si se encuentran en la base de datos registrar la id de usuario
      $admin_user = $username;
      session_register("admin_user");
    }
    else
    {
      // login incorrecto
      do_html_header("Problema:");
      echo "No has podido hacer logged in.
            Debes estar logueado para ver esta p�gina.<br>";
      do_html_url("login.php", "Login");
      do_html_footer();
      exit;
    }
}

do_html_header("Administraci�n");
if (check_admin_user())
  display_admin_menu();
else
  echo "No est�s autorizado a entrar en el �rea de administraci�n.";

do_html_footer();

?>