<?

// incluye archivos de funciones para esta aplicaci�n
require_once("book_sc_fns.php");
session_start();

do_html_header("A�adir una categor�a");
if (check_admin_user())
{
  if (filled_out($HTTP_POST_VARS))
  {
    if(insert_category($catname))
      echo "Categor�a '$catname' ha sido a�adida a la base de datos.<br>";
    else
      echo "Categor�a '$catname' no ha podido ser a�adida a la base de datos.<br>";
  }
  else
    echo "No has cubierto el formulario.  Prueba de nuevo, por favor.";
  do_html_url("admin.php", "Volver al men� de administraci�n");
}
else
  echo "No est�s autorizado a ver esta p�gina.";

do_html_footer();

?>