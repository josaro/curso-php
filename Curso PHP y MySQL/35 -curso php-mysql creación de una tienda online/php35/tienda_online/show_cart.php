<?
  include ('book_sc_fns.php');
  // El carrito de compra necesita sesiones, as� que iniciar una.
  session_start();

  if($new)
  {
    //nuevo art�culo seleccionado
    if(!session_is_registered("cart"))
    {
      $cart = array();
      session_register("cart");
      $items = 0;
      session_register("items");
      $total_price = "0.00";
      session_register("total_price");
    }
    if($cart[$new])
      $cart[$new]++;
    else
      $cart[$new] = 1;
    $total_price = calculate_price($cart);
    $items = calculate_items($cart);

  }
  if($save)
  {
    foreach ($cart as $isbn => $qty)
    {
      if($$isbn=="0")
        unset($cart[$isbn]);
      else
        $cart[$isbn] = $$isbn;
    }
    $total_price = calculate_price($cart);
    $items = calculate_items($cart);
  }

  do_html_header("Tu carro de compra");

  if($cart&&array_count_values($cart))
    display_cart($cart);
  else
  {
    echo "<p>No hay art�culos en tu carro";
    echo "<hr>";
  }
  $target = "index.php";

  // si hemos a�adido un art�culo al carro, continuar comprando en esa categor�a
  if($new)
  {
    $details =  get_book_details($new);
    if($details["catid"])
      $target = "show_cat.php?catid=".$details["catid"];
  }
  display_button($target, "continue-shopping", "Continue Shopping");
  $path = $PHP_SELF;
  $path = str_replace("show_cart.php", "", $path);
  display_button("http://".$SERVER_NAME.$path."checkout.php", "go-to-checkout", "Go To Checkout");
  do_html_footer();
?>