<?
  include ('book_sc_fns.php');
  // el carrito de compra necesita  sesiones, as� que iniciamos una
  session_start();
  do_html_header("Bienvenido a la Librer�a Online");

  echo "<p>Por favor elija una categor�a:</p>";

  // recuperar categor�as de la base de datos
  $cat_array = get_categories();

  // mostrar como enlaces a las p�ginas de las categor�as
  display_categories($cat_array);

  // si est� logueado como admin, mostrar a�adir, borrar, editar enlaces cat
  if(session_is_registered("admin_user"))
  {
    display_button("admin.php", "admin-menu", "Admin Menu");
  }

  do_html_footer();
?>