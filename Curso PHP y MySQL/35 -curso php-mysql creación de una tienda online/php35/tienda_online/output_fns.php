<?

function do_html_header($title = '')
{
  // print an HTML header

  // declare the session variables we want access to inside the function
  global $total_price;
  global $items;
  if(!$items) $items = "0";
  if(!$total_price) $total_price = "0.00";
?>
  <html>
  <head>
    <title><?=$title?></title>
    <style>
      h2 { font-family: Arial, Helvetica, sans-serif; font-size: 22px; color = red; margin = 6px }
      body { font-family: Arial, Helvetica, sans-serif; font-size: 13px }
      li, td { font-family: Arial, Helvetica, sans-serif; font-size: 13px }
      hr { color: #FF0000; width=70%; text-align=center}
      a { color: #000000 }
    </style>
  </head>
  <body>
  <table width=100% border=0 cellspacing = 0 bgcolor=#cccccc>
  <tr>
  <td rowspan = 2>
  <a href = "index.php"><img src="images/Book-O-Rama.gif" alt="Bookorama" border=0
       align=left valign=bottom height = 60 width = 247></a>
  </td>
  <td align = right valign = bottom>
  <? if(session_is_registered("admin_user"))
       echo "&nbsp;";
     else
       echo "Total Items = $items";
  ?>
  </td>
  <td align = right rowspan = 2 width = 135>
  <? if(session_is_registered("admin_user"))
       display_button("logout.php", "log-out", "Log Out");
     else
       display_button("show_cart.php", "view-cart", "View Your Shopping Cart");
  ?>
  </tr>
  <tr>
  <td align = right valign = top>
  <? if(session_is_registered("admin_user"))
       echo "&nbsp;";
     else
       echo "Precio Total = $".number_format($total_price,2);
  ?>
  </td>
  </tr>
  </table>
<?
  if($title)
    do_html_heading($title);
}

function do_html_footer()
{
  // print an HTML footer
?>
  </body>
  </html>
<?
}

function do_html_heading($heading)
{
  // print heading
?>
  <h2><?=$heading?></h2>
<?
}

function do_html_URL($url, $name)
{
  // output URL as link and br
?>
  <a href="<?=$url?>"><?=$name?></a><br>
<?
}

function display_categories($cat_array)
{
  if (!is_array($cat_array))
  {
     echo "No hay categor�as actualmente disponibles<br>";
     return;
  }
  echo "<ul>";
  foreach ($cat_array as $row)
  {
    $url = "show_cat.php?catid=".($row["catid"]);
    $title = $row["catname"];
    echo "<li>";
    do_html_url($url, $title);
  }
  echo "</ul>";
  echo "<hr>";
}

function display_books($book_array)
{
  //display all books in the array passed in
  if (!is_array($book_array))
  {
     echo "<br>No books currently available in this category<br>";
  }
  else
  {
    //create table
    echo "<table width = \"100%\" border = 0>";

    //create a table row for each book
    foreach ($book_array as $row)
    {
      $url = "show_book.php?isbn=".($row["isbn"]);
      echo "<tr><td>";
      if (@file_exists("images/".$row["isbn"].".jpg"))
      {
        $title = "<img src=\"images/".($row["isbn"]).".jpg\" border=0>";
        do_html_url($url, $title);
      }
      else
      {
        echo "&nbsp;";
      }
      echo "</td><td>";
      $title =  $row["title"]." by ".$row["author"];
      do_html_url($url, $title);
      echo "</td></tr>";
    }
    echo "</table>";
  }
  echo "<hr>";
}

function display_book_details($book)
{
  // display all details about this book
  if (is_array($book))
  {
    echo "<table><tr>";
    //display the picture if there is one
    if (@file_exists("images/".($book["isbn"]).".jpg"))
    {
      $size = GetImageSize("images/".$book["isbn"].".jpg");
      if($size[0]>0 && $size[1]>0)
        echo "<td><img src=\"images/".$book["isbn"].".jpg\" border=0 ".$size[3]."></td>";
    }
    echo "<td><ul>";
    echo "<li><b>Autor:</b> ";
    echo $book["author"];
    echo "<li><b>ISBN:</b> ";
    echo $book["isbn"];
    echo "<li><b>Nuestro Precio:</b> ";
    echo number_format($book["price"], 2);
    echo "<li><b>Descripci�n:</b> ";
    echo $book["description"];
    echo "</ul></td></tr></table>";
  }
  else
    echo "Los detalles de este libro no pueden ser mostrados en este momento.";
  echo "<hr>";
}

function display_checkout_form()
{
  //display the form that asks for name and address
?>
  <br>
  <table border = 0 width = 100% cellspacing = 0>
  <form action = purchase.php method = post>
  <tr><th colspan = 2 bgcolor="#cccccc">Su informaci�n</th></tr>
  <tr>
    <td>Nombre</td>
    <td><input type = text name = name value = "" maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td>Direcci�n</td>
    <td><input type = text name = address value = "" maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td>Ciudad/Suburbio</td>
    <td><input type = text name = city value = "" maxlength = 20 size = 40></td>
  </tr>
  <tr>
    <td>Estado/Provincia</td>
    <td><input type = text name = state value = "" maxlength = 20 size = 40></td>
  </tr>
  <tr>
    <td>C�digo Postal o C�digo Zip</td>
    <td><input type = text name = zip value = "" maxlength = 10 size = 40></td>
  </tr>
  <tr>
    <td>Pa�s</td>
    <td><input type = text name = country value = "" maxlength = 20 size = 40></td>
  </tr>
  <tr><th colspan = 2 bgcolor="#cccccc">Direcci�n de env�o (dejar en blanco si es el de arriba)</th></tr>
  <tr>
    <td>Nombre</td>
    <td><input type = text name = ship_name value = "" maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td>Direcci�n</td>
    <td><input type = text name = ship_address value = "" maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td>Ciudad/Suburbio</td>
    <td><input type = text name = ship_city value = "" maxlength = 20 size = 40></td>
  </tr>
  <tr>
    <td>Estado/Provincia</td>
    <td><input type = text name = ship_state value = "" maxlength = 20 size = 40></td>
  </tr>
  <tr>
    <td>C�digo Postal o C�digo Zip</td>
    <td><input type = text name = ship_zip value = "" maxlength = 10 size = 40></td>
  </tr>
  <tr>
    <td>Pa�s</td>
    <td><input type = text name = ship_country value = "" maxlength = 20 size = 40></td>
  </tr>
  <tr>
    <td colspan = 2 align = center>
      <b>Por favor pulsar compar para confirmar su compra,
         o Continuar comprando para a�adir o eliminar art�culos</b>
     <? display_form_button("purchase", "Purchase These Items"); ?>
    </td>
  </tr>
  </form>
  </table><hr>
<?
}

function display_shipping($shipping)
{
  // display table row with shipping cost and total price including shipping
  global $total_price;
?>
  <table border = 0 width = 100% cellspacing = 0>
  <tr><td align = left>Env�o</td>
      <td align = right> <?=number_format($shipping, 2); ?></td></tr>
  <tr><th bgcolor="#cccccc" align = left>TOTAL INCLUIDO ENV�O</th>
      <th bgcolor="#cccccc" align = right>$<?=number_format($shipping+$total_price, 2); ?></th>
  </tr>
  </table><br>
<?
}

function display_card_form($name)
{
  //display form asking for credit card details
?>
  <table border = 0 width = 100% cellspacing = 0>
  <form action = process.php method = post>
  <tr><th colspan = 2 bgcolor="#cccccc">Detalles Tarjeta Cr�dito</th></tr>
  <tr>
    <td>Tipo</td>
    <td><select name = card_type><option>VISA<option>MasterCard<option>American Express</select></td>
  </tr>
  <tr>
    <td>N�mero</td>
    <td><input type = text name = card_number value = "" maxlength = 16 size = 40></td>
  </tr>
  <tr>
    <td>C�dig AMEX code (si se requiere)</td>
    <td><input type = text name = amex_code value = "" maxlength = 4 size = 4></td>
  </tr>
  <tr>
    <td>Fecha Expiraci�n</td>
    <td>Mes <select name = card_month><option>01<option>02<option>03<option>04<option>05<option>06<option>07<option>08<option>09<option>10<option>11<option>12</select>
    a�o <select name = card_year><option>00<option>01<option>02<option>03<option>04<option>05<option>06<option>07<option>08<option>09<option>10</select></td>
  </tr>
  <tr>
    <td>Nombre en Tarjeta</td>
    <td><input type = text name = card_name value = "<?=$name; ?>" maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td colspan = 2 align = center>
      <b>Por favor pulsa comprar para confirmar tu compra,
         o Continuar Comprando para a�adir o eliminar art�culos</b>
     <? display_form_button("purchase", "Purchase These Items"); ?>
    </td>
  </tr>
  </table>
<?
}



function display_cart($cart, $change = true, $images = 1)
{
  // display items in shopping cart
  // optionally allow changes (true or false)
  // optionally include images (1 - yes, 0 - no)

  global $items;
  global $total_price;

  echo "<table border = 0 width = 100% cellspacing = 0>
        <form action = show_cart.php method = post>
        <tr><th colspan = ". (1+$images) ." bgcolor=\"#cccccc\">Item</th>
        <th bgcolor=\"#cccccc\">Price</th><th bgcolor=\"#cccccc\">Quantity</th>
        <th bgcolor=\"#cccccc\">Total</th></tr>";

  //display each item as a table row
  foreach ($cart as $isbn => $qty)
  {
    $book = get_book_details($isbn);
    echo "<tr>";
    if($images ==true)
    {
      echo "<td align = left>";
      if (file_exists("images/$isbn.jpg"))
      {
         $size = GetImageSize("images/".$isbn.".jpg");
         if($size[0]>0 && $size[1]>0)
         {
           echo "<img src=\"images/".$isbn.".jpg\" border=0 ";
           echo "width = ". $size[0]/3 ." height = " .$size[1]/3 . ">";
         }
      }
      else
         echo "&nbsp;";
      echo "</td>";
    }
    echo "<td align = left>";
    echo "<a href = \"show_book.php?isbn=".$isbn."\">".$book["title"]."</a> by ".$book["author"];
    echo "</td><td align = center>$".number_format($book["price"], 2);
    echo "</td><td align = center>";
    // if we allow changes, quantities are in text boxes
    if ($change == true)
      echo "<input type = text name = \"$isbn\" value = $qty size = 3>";
    else
      echo $qty;
    echo "</td><td align = center>$".number_format($book["price"]*$qty,2)."</td></tr>\n";
  }
  // display total row
  echo "<tr>
          <th colspan = ". (2+$images) ." bgcolor=\"#cccccc\">&nbsp;</td>
          <th align = center bgcolor=\"#cccccc\">
              $items
          </th>
          <th align = center bgcolor=\"#cccccc\">
              \$".number_format($total_price, 2).
          "</th>
        </tr>";
  // display save change button
  if($change == true)
  {
    echo "<tr>
            <td colspan = ". (2+$images) .">&nbsp;</td>
            <td align = center>
              <input type = hidden name = save value = true>
              <input type = image src = \"images/save-changes.gif\"
                     border = 0 alt = \"Save Changes\">
            </td>
            <td>&nbsp;</td>
        </tr>";
  }
  echo "</form></table>";
}

function display_login_form()
{
  // dispaly form asking for name and password
?>
  <form method=post action="admin.php">
  <table bgcolor=#cccccc>
   <tr>
     <td>Nombre Usuario:</td>
     <td><input type=text name=username></td></tr>
   <tr>
     <td>Contrase�a:</td>
     <td><input type=password name=passwd></td></tr>
   <tr>
     <td colspan=2 align=center>
     <input type=submit value="Log in"></td></tr>
   <tr>
 </table></form>
<?
}

function display_admin_menu()
{
?>
<br>
<a href="index.php">Ir al sitio principal</a><br>
<a href="insert_category_form.php">A�adir una nueva categor�a</a><br>
<a href="insert_book_form.php">A�adir un nuevo libro</a><br>
<a href="change_password_form.php">Cambiar la contrase�a de Admin</a><br>
<?

}

function display_button($target, $image, $alt)
{
  echo "<center><a href=\"$target\"><img src=\"images/$image".".gif\"
           alt=\"$alt\" border=0 height = 50 width = 135></a></center>";
}

function display_form_button($image, $alt)
{
  echo "<center><input type = image src=\"images/$image".".gif\"
           alt=\"$alt\" border=0 height = 50 width = 135></center>";
}

?>