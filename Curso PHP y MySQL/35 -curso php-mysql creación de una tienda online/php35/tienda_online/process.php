<?
  include ('book_sc_fns.php');
  // El carro de compra necesita sesiones, as� que empezar una
  session_start();

  do_html_header("Caja de pago");

  if($cart&&$card_type&&$card_number&&$card_month&&$card_year&&$card_name )
  {
    //mostrar carro, no permitir cambios y sin im�genes
    display_cart($cart, false, 0);

    display_shipping(calculate_shipping_cost());

    if(process_card($HTTP_POST_VARS))
    {
      //Vaciar carro de compra
      session_destroy();
      echo "Gracias por confiar en nosotros.  Su pedido ha sido tramitado.";
      display_button("index.php", "continue-shopping", "Continue Shopping");
    }
    else
    {
      echo "No hemos podido procesar su tarjeta, Por favor contacte con los emisores de la tarjeta o int�ntalo de nuevo.";
      display_button("purchase.php", "back", "Back");
    }
  }
  else
  {
    echo "No has cubierto todos los campos, por favor, int�ntalo de nuevo.<hr>";
    display_button("purchase.php", "back", "Back");
  }

  do_html_footer();
?>