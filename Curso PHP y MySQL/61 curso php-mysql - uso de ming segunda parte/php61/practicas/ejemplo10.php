<?php
$myMovie = new SWFMovie();
$myMovie->setDimension(400, 300);
$myMovie->setRate(5);
$myMovie->setBackground(200, 200, 200);

// Crear un nuevo objeto morph
$morph = new SWFMorph();

// Crear una nueva forma y configurar el estilo de l�nea
$square = new SWFShape();
$rectangle = new SWFShape();

$square = $morph->getShape1();
$square->setLine(5, 0, 0, 0, 255);

$rectangle = $morph->getShape2();
$rectangle->setLine(5, 0, 0, 0, 255);

// Dibujar un cuadrado
$square->movePenTo(1, 1);
$square->drawLineTo(61, 1);
$square->drawLineTo(61, 61);
$square->drawLineTo(1, 61);
$square->drawLineTo(1, 1);

// Dibujar un rect�ngulo
$rectangle->movePenTo(1, 1);
$rectangle->drawLineTo(161, 1);
$rectangle->drawLineTo(161, 61);
$rectangle->drawLineTo(1, 61);
$rectangle->drawLineTo(1, 1);

$morphHandle = $myMovie->add($morph);
$morphHandle->moveTo(100, 100);



for($i = 0.0; $i < 1.0; $i += 0.1)
{
    $morphHandle->setRatio($i);
    $myMovie->nextFrame();
	
	
}

// Ahora mostrar la pel�cula
header("Content-type:application/x-shockwave-flash");
$myMovie->output();

?>