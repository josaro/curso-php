<?php
$myMovie = new SWFMovie();
$myMovie->setDimension(400, 300);
$myMovie->setRate(30);
$myMovie->setBackground(200, 200, 200);

// Crear una nueva forma y configurar el estilo de l�nea
$square = new SWFShape();
$square->setLine(5, 0, 0, 0, 255);

// Abrir un gr�fico y leer los datos en un buffer
$fp = fopen("fondo1.png", "rb");
$data = fread($fp, filesize("fondo3.png"));

// Crear una nueva imagen de mapa de bits
$bitmap = new SWFBitmap($data);

// A�adir el relleno a la forma
$fill = $square->addFill($bitmap, SWFFILL_CLIPPED_BITMAP);
$square->setRightFill($fill);

// Dibujar un cuadrado
$square->movePenTo(40, 20);
$square->drawLineTo(240, 20);
$square->drawLineTo(240, 170);
$square->drawLineTo(40, 170);
$square->drawLineTo(40, 20);

// Ahora a�adir las formas a la pel�cula
$myMovie->add($square);

// Ahora mostrar la pel�cula
header("Content-type:application/x-shockwave-flash");
$myMovie->output();
?>