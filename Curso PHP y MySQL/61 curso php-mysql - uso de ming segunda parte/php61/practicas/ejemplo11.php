<?php
$myMovie = new SWFMovie();
$myMovie->setDimension(400, 300);
$myMovie->setRate(5);
$myMovie->setBackground(200, 200, 200);

// Crear un nuevo objeto morph
$morph = new SWFMorph();

// Crear una nueva forma y configurar el estilo de l�nea
$square = new SWFShape();
$square->setLine(5, 0, 0, 0, 255);

// Dibujar el cuadrado
$square->movePenTo(1, 1);
$square->drawLineTo(61, 1);
$square->drawLineTo(61, 61);
$square->drawLineTo(1, 61);
$square->drawLineTo(1, 1);

// A�adir la forma al clip de pel�cula de modo que podamos acceder a algunas propiedades m�s tarde
$shape = new SWFSprite();
$shape->add($square);
$shape->nextFrame();

// Crear una caja de texto para mostrar alg�n texto
$textbox = new SWFTextField(SWFTEXTFIELD_DRAWBOX);
$textbox->setBounds(100, 100);
$textbox->setFont(new SWFFont("_sans"));
$textbox->setColor(255, 0, 0);

// Ahora dar a la caja de texto un nombre para poder referenciarlo
$textbox->setName("myTextBox");

// Mover la caja de texto para a�adirla a un sprite
$container = new SWFSprite();
$container->add($textbox);

// Mover al siguiente fotograma en el sprite para mostrarlo
$container->nextFrame();

// A�adir el clip de pel�cula al cuadrado
$shapeHandle = $myMovie->add($shape);
$shapeHandle->moveTo(125, 45);
$shapeHandle->setName("shape");

$containerHandle = $myMovie->add($container);
$containerHandle->moveTo(200, 100);
$containerHandle->setName("container");

$myMovie->nextFrame();
$myMovie->add(new SWFAction("_root.container.myTextBox = _root.shape._x +
', ' + _root.shape._y;"));

// Ahora mostrar la pel�cula
header("Content-type:application/x-shockwave-flash");
$myMovie->output();
?>