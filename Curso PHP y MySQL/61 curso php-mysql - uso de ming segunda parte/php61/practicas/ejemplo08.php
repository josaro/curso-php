<?php
$myMovie = new SWFMovie();
$myMovie->setDimension(400, 300);
$myMovie->setRate(30);
$myMovie->setBackground(200, 200, 200);

// Crear una nueva forma y configurar el estilo de l�nea
$square = new SWFShape();
$square->setLine(5, 0, 0, 0, 255);

// Dibujar un cuadrado
$square->movePenTo(1, 1);
$square->drawLineTo(61, 1);
$square->drawLineTo(61, 61);
$square->drawLineTo(1, 61);
$square->drawLineTo(1, 1);

// Ahora a�adir las formas a la pel�cula
$squareHandle = $myMovie->add($square);
// Mover la forma un poco
$squareHandle->moveTo(30, 100);
// Rotar la forma 45 grados
$squareHandle->rotate(45);

// Ahora mostrar la pel�cula
header("Content-type:application/x-shockwave-flash");
$myMovie->output();
?>