<?php
$myMovie = new SWFMovie();
$myMovie->setDimension(400, 300);
$myMovie->setRate(30);
$myMovie->setBackground(200, 200, 200);

// Crear una nueva forma y configurar el estilo de l�nea
$square = new SWFShape();
$square->setLine(5, 0, 0, 0, 255);  
// Crear un nuevo relleno de Gradiente
$gradient = new SWFGradient();
$gradient->addEntry(0.0, 255, 0, 0);
$gradient->addEntry(0.5, 255, 255, 255);

// A�adir el relleno a la forma
$fill = $square->addFill($gradient, SWFFILL_RADIAL_GRADIENT);
$square->setRightFill($fill);

// Dibujar un cuadrado
$square->movePenTo(40, 20);
$square->drawLineTo(340, 20);
$square->drawLineTo(340, 220);
$square->drawLineTo(40, 220);
$square->drawLineTo(40, 20);

// Ahora a�adir la forma a la pel�cula
$myMovie->add($square);

// Ahora mostrar la pel�cula
header("Content-type:application/x-shockwave-flash");
$myMovie->output();
?>