<?
/*******************************************
  Petici�n a la base de datos para obtener la informaci�n de la encuesta
*******************************************/
// hacer log in en la base de datos
if (!$db_conn = @mysql_connect("62.149.150.14", "Sql18226", "ZhVR9Mce"))
{
  echo "No se puede conectar a la base de datos<br>";
  exit;
};
@mysql_select_db("Sql18226_1");

if (!empty($vote))  // Si han cubierto bien el formulario
{
  $vote = addslashes($vote);
  $query = "update poll_results
            set num_votes = num_votes + 1
            where candidate = '$vote'";
  if(!($result = @mysql_query($query, $db_conn)))
  {
    echo "No se ha podido conectar a la base de datos<br>";
    exit;
  }
};

// obtener los resultados actuales de la encuentas, independientemente de lo que haya votado
$query = "select * from poll_results";
if(!($result = @mysql_query($query, $db_conn)))
{
  echo "No se puede conectar a la base de datos<br>";
  exit;
}
$num_candidates = mysql_num_rows($result);

// calcular el n�mero total de votos hasta ahora
$total_votes=0;
while ($row = mysql_fetch_object ($result))
{
    $total_votes +=  $row->num_votes;
}
mysql_data_seek($result, 0);  // resetear el resultado


/*******************************************
  C�lculo inicial para el gr�fico
*******************************************/
// configurar constantes
$width=500;        // ancho de la imagen en pixeles - encajar� en 640x480
$left_margin = 50; // espacio a dejar a la izquierda de la imagen
$right_margin= 50; // lo mismo para la derecha
$bar_height = 40;
$bar_spacing = $bar_height/2;
$font = "arial.ttf";
$title_size= 16; // puntos
$main_size= 12; // puntos
$small_size= 12; // puntos
$text_indent = 10; // posici�n para las etiquetas de texto a la izquierda

// configurar el punto inicial desde el cual dibujar
$x = $left_margin + 60;  // colocar la l�nea de base para dibujar del gr�fico
$y = 50;                 // lo mismo
$bar_unit = ($width-($x+$right_margin)) / 100;   // un "punto" en el gr�fico

// c�lcula el alto del gr�fico -  barras m�s espacios m�s el margen
$height = $num_candidates * ($bar_height + $bar_spacing) + 50;

/*******************************************
  Configurar la imagen base
*******************************************/
// crear un lienzo en blanco
$im = imagecreate($width,$height);

// Asignar colores
$white=ImageColorAllocate($im,255,255,255);
$blue=ImageColorAllocate($im,0,64,128);
$black=ImageColorAllocate($im,0,0,0);
$pink = ImageColorAllocate($im,255,78,243);

$text_color = $black;
$percent_color = $black;
$bg_color = $white;
$line_color = $black;
$bar_color = $blue;
$number_color = $pink;

// Crear "lienzo" para dibujar
ImageFilledRectangle($im,0,0,$width,$height,$bg_color);

// Dibujar borde en torno al lienzo
ImageRectangle($im,0,0,$width-1,$height-1,$line_color);

// A�adir T�tulo
$title = "Resultados Sondeo";
$title_dimensions = ImageTTFBBox($title_size, 0, $font, $title);
$title_length = $title_dimensions[2] - $title_dimensions[0];
$title_height = abs($title_dimensions[7] - $title_dimensions[1]);
$title_above_line = abs($title_dimensions[7]);
$title_x = ($width-$title_length)/2;  // centrarlo en x
$title_y = ($y - $title_height)/2 + $title_above_line; // centrarlo en y
ImageTTFText($im, $title_size, 0, $title_x, $title_y,
             $text_color, $font, $title);

// Dibujar una l�nea de base un poco por encima de la primera localizaci�n de la barra
// a un poco por debajo de la �ltima
ImageLine($im, $x, $y-5, $x, $height-15, $line_color);

/*******************************************
  Dibujar los datos en el gr�fico
*******************************************/
// Obtener cada l�nea de los datos de la base de datos y dibujar las barras correspondientes
while ($row = mysql_fetch_object ($result))
{
  if ($total_votes > 0)
    $percent = intval(round(($row->num_votes/$total_votes)*100));
  else
    $percent = 0;

  // muestra el tanto por ciento para este valor
  ImageTTFText($im, $main_size, 0, $width-30, $y+($bar_height/2),
               $percent_color, $font, $percent."%");
  if ($total_votes > 0)
    $right_value = intval(round(($row->num_votes/$total_votes)*100));
  else
    $right_value = 0;

  // tama�o de barra para este valor
  $bar_length = $x + ($right_value * $bar_unit);

  // dibujar barra para este valor
  ImageFilledRectangle($im, $x, $y-2, $bar_length, $y+$bar_height, $bar_color);

  // dibujar t�tulo para este valor
  ImageTTFText($im, $main_size, 0, $text_indent, $y+($bar_height/2),
               $text_color, $font, "$row->candidate");

  // dibujar contorno mostrando 100%
  ImageRectangle($im, $bar_length+1, $y-2,
                ($x+(100*$bar_unit)), $y+$bar_height, $line_color);

  // mostrar n�meros
  ImageTTFText($im, $small_size, 0, $x+(100*$bar_unit)-50, $y+($bar_height/2),
               $number_color, $font, $row->num_votes."/".$total_votes);

  // hacia abajo a la siguiente barra
  $y=$y+($bar_height+$bar_spacing);
}

/*******************************************
  Mostrar imagen
*******************************************/
Header("Content-type:  image/png");
ImagePng($im);


/*******************************************
  Limpiar
*******************************************/
ImageDestroy($im);
?>