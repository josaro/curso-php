README for the GoogleIt class
by Umang Beri

1.0 What is a GoogleIt?
1.1 What are the applications for CAPTCHAs?
1.2 What are future plans?
2.0 How to use the class?
3.0 Author
3.1 License
4.0 Version

*** 1.0 What is a GoogleIt?
GoogleIt gets the first headline that is spat out by google 
news based on the topic you wanted.

*** 1.1 What are the applications for GoogleIt?
At the moment, you can just get the latest headline (ranging from 1 to 10 articles). More types of 
versions will be coming out soon.

*** 1.2 This was originally made for one of my sites and I figured I 
would share it and changed it around so more people could use it. Since
this project was more of a by chance effort than a planned one, features
are low, so added features in the future will look like:
i.  	Choose the number of headlines you want to view (Done)
ii.	Other News Search Engines will be made as well

If you have any more suggestions, email them to me at 
umang@duniyadnd.ithium.net  

*** 2.0 How to use the class?
Three lines of code:

//Include the class file
require (GoogleClass.php);

//First you specify that it is an object
$blah = new GoogleIt("Change Text Here");

//Then you tell it I want to print out whatever $blah->GoogleD finds. 
echo $blah->GoogleD();

/*Note: For the above, you can also put in a number ranging 1 to 10. Default is 10. This tells the class how many articles you want shown.

eg.
*/
echo $blah->GoogleD(5);     //list 5 stories

There are now two paramaters, which you can treat 
like a string, which will get 
the headlines from news.Google.com
The string can be of any length.

Google Tags were removed so that the topics can be shifted around so that 
it can suit any site.

*** 3.0 Author
Umang Beri
umang@duniyadnd.ithium.net

http://duniyadnd.ithium.net
http://www.footymania.com

*** 3.1 License
GNU General Public License (Version 2, June 1991)

This program is free software; you can redistribute
it and/or modify it under the terms of the GNU
General Public License as published by the Free
Software Foundation; either version 2 of the License,
or (at your option) any later version.

This Readme.txt file Must be distributed along with 
the original/modified files.

This program is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License
for more details @ http://www.gnu.org/licenses/gpl.txt

Give credit to Google whenever you using their service.

*** 4.0 License
Version: 1.2