<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head profile="http://gmpg.org/xfn/11">
	<title>Addicted to Feedburner by John Nunemaker</title>
</head>

<body>
<div id="wrapper">
	<h1>Addicted to Feedburner by John Nunemaker</h1>
	<?php 
		// include the class
		require_once 'class.feedburner.php';
		
		// create new feedburner instance
		$fb =& new feedburner('burnthisrss2');
	?>
	
	<h2>Code Sample for Yesterday's Top Ten Items</h2>
	
	<?php
	echo '<div>' . highlight_file('code/getitemdata.txt', true) . '</div>';
	$result = $fb->getItemData();
	if ($fb->isError()) {
		echo $fb->getErrorMsg();
	} else {
		$items = $result['entries'][1]['items'];
		if (count($items) > 0) {
			echo '<h2>Example Output</h2>';
			echo '<table cellspacing="0" style="width:300px;">
					<caption>Top Ten Items Yesterday</caption>
					<thead>
						<th>Item</th>
						<th>Hits</th>
						<th>Clicks</th>
					</thead>
					<tbody>';
			for($i=1; $i<=10; $i++) {
				echo '<tr>
						<td><a href="' . $items[$i]['url'] . '">' . $items[$i]['title'] . '</a></td>
						<td>' . $items[$i]['itemviews'] . '</td>
						<td>' . $items[$i]['clickthroughs'] . '</td>
					</tr>';
			}
			echo '</tbody>
				</table>';
		}			
	}
	?>
</div>
</body>
</html>