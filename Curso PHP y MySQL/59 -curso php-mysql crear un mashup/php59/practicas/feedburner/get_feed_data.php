<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head profile="http://gmpg.org/xfn/11">
	<title>Addicted to Feedburner by John Nunemaker</title>
</head>

<body>
<div id="wrapper">
	<h1>Addicted to Feedburner by John Nunemaker</h1>
	<?php 
	
	// include the feedburner class
	require_once 'class.feedburner.php';
	
	// create new feedburner instance
	$fb =& new feedburner('burnthisrss2');
	?>
	<h2>Code Sample for Yesterday's Stats</h2>
	<?php
	
	$info = $fb->getFeedData();
	if ($fb->isError()) {
		echo $fb->getErrorMsg();
	} else {
		$entries = $info['entries'][1];
		echo '<h2>Example Output</h2>';
		echo "<h3>Yesterday's Stats</h3>";
		echo '<ul>
				<li>Circulation: ' . $entries['circulation'] . '</li>
				<li>Hits: ' . $entries['hits'] . '</li>
			</ul>';
	}
	?>
	
	<h2>Code Sample for Past Weeks Stats</h2>
	<?php		
	// determines the dates to retrieve data for (now minus 7 days)
	$date_str = date('Y-m-d', strtotime('-7 days', time())) . ',' . date('Y-m-d', time());
	
	// make the call to get the feed data
	$info = $fb->getFeedData(array('dates'=>$date_str));
	if ($fb->isError()) {
		echo $fb->getErrorMsg();
	} else {
		// no error returned; show results
		$entries = $info['entries'];
		if (count($entries) > 0) {
			echo '<h2>Example Output</h2>';
			echo '<table cellspacing="0" style="width:300px;">
					<caption>Stats for Last Week</caption>
					<thead>
						<th>Day</th>
						<th>Circulation</th>
						<th>Hits</th>
					</thead>
					<tbody>';
			foreach($entries as $entry) {
				echo '<tr>
						<td>' . date('l M j', strtotime($entry['date'])) . '</td>
						<td>' . $entry['circulation'] . '</td>
						<td>' . $entry['hits'] . '</td>
					</tr>';
			}
			echo '</tbody>
				</table>';
		}
	}
	?>
</div>
</body>
</html>