<?

// si estamos usando IIS, necesitamos configurar $PHP_AUTH_USER y $PHP_AUTH_PW
if (substr($SERVER_SOFTWARE, 0, 9) == "Microsoft" &&
    !isset($PHP_AUTH_USER) &&
    !isset($PHP_AUTH_PW) &&
    substr($HTTP_AUTHORIZATION, 0, 6) == "Basic "
   )
{
  list($PHP_AUTH_USER, $PHP_AUTH_PW) =
    explode(":", base64_decode(substr($HTTP_AUTHORIZATION, 6)));
}

// Reemplazar esta declaraci�n if con una consulta a una base de datos o similar
if ($PHP_AUTH_USER != "user" || $PHP_AUTH_PW != "pass")
{
  // Visitantes no han dado detalles, o su combinaci�n
  // nombre y contrase�a no es correcta

  header('WWW-Authenticate: Basic realm="Introduce tus datos"');
  if (substr($SERVER_SOFTWARE, 0, 9) == "Microsoft")
    header("Status: 401 Unauthorized");
  else
    header("HTTP/1.0 401 Unauthorized");

  echo "<h1>Entrada Prohibida</h1>";
  echo "No est�s autorizado para acceder a estos recursos.";
}
else
{
  // visitante ha provisto los detalles correctos
  echo "<h1>Bienvenido al �rea restringida</h1>";
  echo "<p>Est�s autorizado para acceder a los recursos restringidos de la Web.";
}

?>