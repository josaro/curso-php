<?php
class Person {

    private $data;



    public function __get($property) {

        if (isset($this->data[$property])) {

            return $this->data[$property];

        } else {

            return false;

        }

    }



    public function __set($property, $value) {

        $this->data[$property] = $value;

    }

}



$rasmus = new Person;

$rasmus->name = 'Pedro Garc�a';

print $rasmus->name;



?>
