<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
</head>

<body>
<?php 

class Address {

    protected $city;

    protected $country;



    public function setCity($city) {

        $this->city = $city;

    }

        

    public function getCity( ) {

        return $this->city;

    }



    public function setCountry($country) {

        $this->country = $country;

    }

        

    public function getCountry( ) {

        return $this->country;

    }
	
	

}



class Person {

    protected $name;

    protected $address;



    public function __construct( ) {

        $this->address = new Address;

    }



    public function setName($name) {

        $this->name = $name;

    }



    public function getName( ) {

        return $this->name;

    }



    public function __call($method, $arguments) {

        if (method_exists($this->address, $method)) {

            return call_user_func_array(

                array($this->address, $method), $arguments);

        }

    }

}


?>
</body>
</html>
