<?php

class Direccion {

    protected $ciudad;

    protected $pais;



    public function setCiudad($ciudad) {

        $this->ciudad = $ciudad;

    }

        

    public function getCiudad( ) {

        return $this->ciudad;

    }



    public function setPais($pais) {

        $this->pais = $pais;

    }

        

    public function getPais( ) {

        return $this->pais;

    }
	
	

}



class Persona {

    protected $nombre;

    protected $direccion;



    public function __construct( ) {

        $this->direccion = new Direccion;

    }



    public function setNombre($nombre) {

        $this->nombre = $nombre;

    }



    public function getNombre( ) {

        return $this->nombre;

    }



    public function __call($method, $arguments) {

        if (method_exists($this->direccion, $method)) {

            return call_user_func_array(

                array($this->direccion, $method), $arguments);

        }

    }

}

$pedro = new Persona;

$pedro->setNombre('Pedro Garc�a');

$pedro->setCiudad('Quito');

$pedro->setPais('Ecuador');



print $pedro->getNombre( ) . ' vive en ' . $pedro->getCiudad( ) . ' capital de ' . $pedro->getPais( ) .  '.';



?>

