<?
// Comprueba que tenemos los datos de variable apropiados
// Las variables son button-text and color

if (empty($button_text) || empty($color))
{
  echo "No se ha podido crear la imagen - el formulario no est� cubierto correctamente";
  exit;
}

// crear una imagen con el fondo correcto y comprueba el tama�o
$im = imagecreatefrompng ("$color-boton.png");

$width_image = ImageSX($im);
$height_image = ImageSY($im);

// Nuestra imagen necesita unos 18 p�xeles de margen desde los bordes de la imagen
$width_image_wo_margins = $width_image - (2 * 18);
$height_image_wo_margins = $height_image - (2 * 18);

// Producir el tama�o de fuente si se ajusta y hacerlo m�s peque�o hasta que lo haga
// Empieza con el mayor tama�o que razonablemente encaje en nuestros botones
$font_size = 33;

do
{
  $font_size--;

  // localizar el tama�o del texto en ese tama�o de fuente
  $bbox=imagettfbbox ($font_size, 0, "arial.ttf", $button_text);

  $right_text = $bbox[2];   // coordenada derecha
  $left_text = $bbox[0];    // coordenada izquierda
  $width_text = $right_text - $left_text;  // �Cu�nto tiene de ancho?
  $height_text = abs($bbox[7] - $bbox[1]);  // �Cu�nto tiene de alto?

} while ( $font_size>8 &&
          ( $height_text>$height_image_wo_margins ||
            $width_text>$width_image_wo_margins )
        );

if ( $height_text>$height_image_wo_margins ||
     $width_text>$width_image_wo_margins )
{
  // no hay tama�o de fuente que se adapte al bot�n
  echo "El texto indicado no encajar� en el bot�n.<BR>";
}
else
{
  // Hemos encontrado un tama�o de fuente que encaja
  // Ahora hacemos el trabajo para ponerlo

  $text_x = $width_image/2.0 - $width_text/2.0;
  $text_y = $height_image/2.0 - $height_text/2.0 ;

  if ($left_text < 0)
      $text_x += abs($left_text);    // a�adir factor para saliente izquierdo

  $above_line_text = abs($bbox[7]);   // �Cu�nto separado de la l�nea de base?
  $text_y += $above_line_text;        // a�adir factor de la l�nea de base

  $text_y -= 2;  // ajustar el factor para la forma de nuestra plantilla

  $white = ImageColorAllocate ($im, 255, 255, 255);

  ImageTTFText ($im, $font_size, 0, $text_x, $text_y, $white, "arial.ttf",
                $button_text);

  Header ("Content-type: image/png");
  ImagePng ($im);
}

ImageDestroy ($im);
?>