<?
// set up image
  $height = 200;
  $width = 200;
  $im = ImageCreate($width, $height);
  $rojo = ImageColorAllocate ($im, 255, 16, 5);
  $azul = ImageColorAllocate ($im, 98, 28, 239);

// draw on image
  ImageFill($im, 0, 0, $rojo);
  ImageLine($im, 0, 0, $width, $height, $azul);
  ImageString($im, 4, 50, 150, "Ventas", $azul);

// output image
  Header ("Content-type: image/png");
  ImagePng ($im);

// clean up
  ImageDestroy($im);
?>