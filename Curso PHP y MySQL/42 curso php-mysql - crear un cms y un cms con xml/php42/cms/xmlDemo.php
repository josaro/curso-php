<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>XML Demo</title>
</head>
<body>
<h1>XML Demo</h1>
<?
//cargar main.xml y examinarlo
$xml = simplexml_load_file("main.xml");
print "<h3>XML original</h3> \n";
$xmlText = $xml->asXML();
$xmlText = htmlentities($xmlText);
print "<pre>$xmlText</pre> \n";
print "<h3>Extraer un elemento</h3> \n";
print $xml->title;
print "<br />";
print "<h3>Extraer como un array</h3> \n";
foreach ($xml->children() as $name => $value){
print "<b>$name:</b> $value<br /> \n";
} // fin foreach
?>
</body>
</html>