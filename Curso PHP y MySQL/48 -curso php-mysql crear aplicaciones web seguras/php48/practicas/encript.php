<?php
// funci�n que crea un nuevo passw para visitante
function new_pw($given) {
return crypt($given)
}
//funci�n que compara un passw dado por un visitante con
//un passw almacenado encriptado.
function verify_pw($given, $stored) {
$salt = substr($stored, 0, CRYPT_SALT_LENGTH);
$given_encrypted = crypt($given, $salt);
return ($stored == $given_encrypted);
}
?>