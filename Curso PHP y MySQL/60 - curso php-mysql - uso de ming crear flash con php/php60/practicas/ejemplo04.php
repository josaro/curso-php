<?php
$myMovie = new SWFMovie();
$myMovie->setDimension(400, 300);
$myMovie->setRate(30);
$myMovie->setBackground(200, 200, 200);

// Dibujar una curva
$curve = new SWFShape();
$curve ->setLine(5, 0, 0, 0, 255);
$curve ->movePenTo(40, 20);
$curve ->drawCurveTo(100, 100, 100, 20);

// Ahora a�adir las formas a la pel�cula
$myMovie->add($curve);

// Ahora mostrar la pel�cula
header("Content-type:application/x-shockwave-flash");
$myMovie->output();
?>