<?php
$myMovie = new SWFMovie();
$myMovie->setDimension(400, 300);
$myMovie->setRate(30);
$myMovie->setBackground(200, 200, 200);

// Crear una nueva forma y a�adir un objeto relleno
$square = new SWFShape();
$square->setLine(5, 0, 0, 0, 255);
$fill = $square->addFill(0, 255, 0);
$square->setRightFill($fill);

// Dibujar un cuadrado
$square->movePenTo(40, 20);
$square->drawLineTo(140, 20);
$square->drawLineTo(140, 120);
$square->drawLineTo(40, 120);
$square->drawLineTo(40, 20);

// Ahora a�adimos la forma a la pel�cula
$myMovie->add($square);

// Ahora mostramos la pel�cula
header("Content-type:application/x-shockwave-flash");
$myMovie->output();
?>