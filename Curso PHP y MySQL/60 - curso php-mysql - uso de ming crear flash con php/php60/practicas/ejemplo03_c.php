<?php
$myMovie = new SWFMovie();
$myMovie->setDimension(400, 300);
$myMovie->setRate(30);
$myMovie->setBackground(200, 200, 200);

// Dibujar una l�nea en el lienzo usando drawLine()
$line1 = new SWFShape();
$line1->setLine(5, 105, 127, 113, 255);
$line1->movePenTo(40, 20);
$line1->drawLine(100, 100);

$line2 = new SWFShape();
$line2->setLine(3, 102, 0, 51, 100);
$line2->movePenTo(40, 20);
$line2->drawLineTo(100, 100);

// Ahora a�adir las formas a la pel�cula
$myMovie->add($line1);
$myMovie->add($line2);

// Ahora mostrar la pel�cula
header("Content-type:application/x-shockwave-flash");
$myMovie->output();
?>